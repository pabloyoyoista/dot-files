;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;       PACKAGES CONFIGURATION       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'package)
(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)

;; list the packages you want
(setq package-list '(company
		     dockerfile-mode
		     flycheck
		     groovy-mode
		     go-mode
		     lsp-ui
		     lsp-mode
		     lsp-treemacs
		     markdown-mode
		     yaml-mode
		     apache-mode
		     hcl-mode
		     ggtags
		     editorconfig))

;; fetch the list of packages available
(unless package-archive-contents
  (package-refresh-contents))

;; install the missing packages
(dolist (package package-list)
  (unless (package-installed-p package)
    (package-install package)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;       STANDARD CONFIGURATION          ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setq inhibit-startup-message t)
(global-display-line-numbers-mode t)

;; For line/column at the bottom of the buffer
(line-number-mode t)
(column-number-mode t)

;; White spaces management
(global-whitespace-mode t)
(add-hook 'before-save-hook 'delete-trailing-whitespace)
(setq whitespace-line-column 80)
(setq whitespace-style
   (quote
    (face tabs trailing space-before-tab lines-tail newline indentation empty space-after-tab tab-mark)))

;; Electric mode
(electric-pair-mode t)

;; Symbolic links
(setq vc-follow-symlinks nil)

;; backup in one place. Flat, no tree structure
(setq backup-directory-alist '(("" . "~/.emacs.d/emacs-backup")))

;; Activate company mode by default
(add-hook 'after-init-hook 'global-company-mode)

;; Enable editorconfig mode
(editorconfig-mode t)

(setq lsp-ui-sideline-show-code-actions t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;    LANGUAGE-SPECIFIC CONFIGURATION    ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Standard Jedi.el (python autocomplete) setting
(add-hook 'python-mode-hook 'jedi:setup)
(setq jedi:complete-on-dot t)

;; Python configuration. 4 spaces on tab
(add-hook 'python-mode-hook
	  (lambda ()
	    (auto-complete-mode t)
	    (setq indent-tabs-mode nil)
	    (setq python-indent-offset 4)
	    (setq tab-width 4)))

(add-hook 'java-mode-hook
	  (lambda ()
	    (setq c-basic-offset 4
		  tab-width 4
		  indent-tabs-mode t)))

;; C - Linux coding style
;;(setq indent-tabs-mode nil)
(setq c-basic-offset 8)
(defun c-lineup-arglist-tabs-only (ignored)
  "Line up argument lists by tabs, not spaces"
  (let* ((anchor (c-langelem-pos c-syntactic-element))
         (column (c-langelem-2nd-pos c-syntactic-element))
         (offset (- (1+ column) anchor))
         (steps (floor offset c-basic-offset)))
    (* (max steps 1)
       c-basic-offset)))
(add-hook 'c-mode-common-hook
          (lambda ()
	    (ggtags-mode 1)
	    ;; Could be interesting to use imenu-list here
	    (setq-local imenu-create-index-function #'ggtags-build-imenu-index)
            ;; Add kernel style
            (c-add-style
             "linux-tabs-only"
             '("linux" (c-offsets-alist
                        (arglist-cont-nonempty
                         c-lineup-gcc-asm-reg
                         c-lineup-arglist-tabs-only))))))
'(c-set-style "linux-tabs-only")
;; Load clang-format if possible
(require 'clang-format)

;; Go configuration
(add-hook 'before-save-hook 'gofmt-before-save)
(require 'company)
(require 'company-go)
(defun lsp-go-install-save-hooks ()
  (add-hook 'before-save-hook #'lsp-format-buffer t t)
  (add-hook 'before-save-hook #'lsp-organize-imports t t))
(add-hook 'go-mode-hook #'lsp-go-install-save-hooks)
(add-hook 'go-mode-hook
	  (lambda ()
	    (company-mode)))
(add-hook 'go-mode-hook 'lsp-deferred)

;; Java configuration
(add-hook 'java-mode-hook
	  (lambda ()
	    (setq c-basic-offset 4
		  tab-width 4
		  indent-tabs-mode t)
	    (subword-mode t)))

;; Autodetect MATLAB files
(add-to-list 'auto-mode-alist '("\\.m" . octave-mode))

;; Markdown configuration
(add-to-list 'auto-mode-alist '("\\.md" . markdown-mode))

;; Fail2ban configuration
(add-to-list 'auto-mode-alist '("\\.local" . conf-unix-mode))

;; Containerfile configuration
(add-to-list 'auto-mode-alist '("Containerfile" . dockerfile-mode))

;; Apache mode
(add-to-list 'auto-mode-alist '("\\.htaccess\\'"   . apache-mode))
(add-to-list 'auto-mode-alist '("httpd\\.conf\\'"  . apache-mode))
(add-to-list 'auto-mode-alist '("srm\\.conf\\'"    . apache-mode))
(add-to-list 'auto-mode-alist '("access\\.conf\\'" . apache-mode))
(add-to-list 'auto-mode-alist '("sites-\\(available\\|enabled\\)/" . apache-mode))

;; Modify whitespaces in plain text modes
(add-to-list 'auto-mode-alist '("\\.log" . text-mode))
(add-hook 'text-mode-hook
	  (lambda ()
	    (setq whitespace-style
		  (quote
		   (face tabs trailing newline indentation empty tab-mark)))))

;; HTML
(add-hook 'sgml-mode-hook
          (lambda ()
            ;; Default indentation to 2, but let SGML mode guess, too.
            (set (make-local-variable 'sgml-basic-offset) 2)
            (sgml-guess-indent)))

;; PO mode
(setq auto-mode-alist
      (cons '("\\.po\\'\\|\\.po\\." . po-mode) auto-mode-alist))
(autoload 'po-mode "po-mode" "Major mode for translators to edit PO files" t)

;; JSON
(add-hook 'json-mode-hook
	  (lambda ()
	    (setq tab-width 4)
	    (setq indent-tabs-mode t)))

;; TS, TSX
(add-to-list 'auto-mode-alist '("\\.tsx" . tsx-ts-mode))
(add-to-list 'auto-mode-alist '("\\.ts" . tsx-ts-mode))
(add-hook 'typescript-ts-base-mode-hook
	  (lambda ()
	    (setq indent-tabs-mode nil)
	    (setq typescript-ts-mode-indent-offset 2)))

;; Tree-Sitter
(setq treesit-language-source-alist
      '((tsx
	 "https://github.com/tree-sitter/tree-sitter-typescript"
	 "master"
	 "tsx/src")
	(typescript
	 "https://github.com/tree-sitter/tree-sitter-typescript"
	 "master"
	 "typescript/src")
	(rust
	 "https://github.com/tree-sitter/tree-sitter-rust"
	 "master"
	 "src")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;         Specific key bindings          ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setq lsp-keymap-prefix "M-l")
(require 'lsp-mode)
(require 'lsp-treemacs)
(define-key lsp-mode-map (kbd "M-l M-e") 'lsp-treemacs-errors-list)
;(define-key lsp-mode-map (kbd "M-.") 'lsp-find-definition)


; To update GTAGS
;(define-key ggtags-mode-map (kbd "C-c g u") 'ggtags-update-tags)
;To directly find other references instead of the definition
;(define-key ggtags-mode-map (kbd "C-c g r") 'ggtags-find-reference)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;         Copyright insertion           ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setq user-name "Pablo Correa Gomez")
(auto-insert-mode)  ;;; Adds hook to find-files-hook
(setq auto-insert-query nil)
;(setq auto-insert-directory "~/Templates/") ;;; Or use custom, *NOTE* Trailing slash important
(define-auto-insert "\.sh"
    '(\n
      "#!/bin/bash" \n
      "#" \n
      "# Copyright (C) " (format-time-string "%Y") " " (symbol-value 'user-name) \n
      "#" \n \n _ ))
(define-auto-insert "\.py"
     '("Short description: "
       "#" \n "# "
       (file-name-nondirectory (buffer-file-name))
       " -- " str \n "#" \n
       "# Copyright (C) " (format-time-string "%Y") " " (symbol-value 'user-name) \n
       "#" \n \n _ ))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;              CUSTOM                   ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["black" "#d55e00" "#009e73" "#f8ec59" "#0072b2" "#cc79a7" "#56b4e9" "white"])
 '(custom-enabled-themes (quote (deeper-blue))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
