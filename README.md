# Personal configuration files
This repo has the goal of storing the configuration files for different programs to ease automation and keep a common configuration in between different computers
## Installation
### emacs
```bash
ln -s $(pwd)/emacs /etc/emacs/default.el
```
For go mode, there is a bug in the most interesting part, which is go-remove-unused imports. Get [https://github.com/krismolendyke/go-mode.el/commit/7a16eb6c65f0a713ab36ad285e87ed40dab9e537](this fix), apply it to ~/.emacs/elpa/go*/go-mode.el and then from emacs run `byte-compile-directory`
### git
```bash
ln -s $(pwd)/gitconfig /etc/gitconfig
```
### tmux
```bash
ln -s $(pwd)/tmux.conf /etc/tmux.conf
sudo apt-get install tmux-plugin-manager # If not available download from debian
```
Afterwards inside tmux do `Prefix+I` to download and install plugins
